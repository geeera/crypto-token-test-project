import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CryptoTokenFormComponent } from './components/crypto-token-form/crypto-token-form.component';
import { TokensListComponent } from './components/tokens-list/tokens-list.component';
import { TokenComponent } from './components/token/token.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    CryptoTokenFormComponent,
    TokensListComponent,
    TokenComponent
  ],
    imports: [
        BrowserModule,
        ReactiveFormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
