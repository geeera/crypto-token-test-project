import {Component, OnDestroy, OnInit} from '@angular/core';
import {CryptoTokenService, IToken} from "./services/crypto-token.service";
import {Observable, Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  tokens$!: Observable<IToken[]>;
  private destroy$ = new Subject();

  constructor(
    private cryptoService: CryptoTokenService
  ) {}

  ngOnInit() {
    this.tokens$ = this.cryptoService.observe;
  }

  createToken(token: IToken) {
    this.cryptoService.createToken(token)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
