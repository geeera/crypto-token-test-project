import {Component, Input, OnInit} from '@angular/core';
import {IToken} from "../../services/crypto-token.service";

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.scss']
})
export class TokenComponent implements OnInit {
  @Input() token!: IToken;

  constructor() { }

  ngOnInit(): void {
  }

}
