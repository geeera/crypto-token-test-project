import {Component, Input, OnInit} from '@angular/core';
import {IToken} from "../../services/crypto-token.service";

@Component({
  selector: 'app-tokens-list',
  templateUrl: './tokens-list.component.html',
  styleUrls: ['./tokens-list.component.scss']
})
export class TokensListComponent implements OnInit {
  @Input() tokens: IToken[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}
