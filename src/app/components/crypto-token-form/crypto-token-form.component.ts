import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {BlockChain, BLOCKCHAIN, IToken} from "../../services/crypto-token.service";

@Component({
  selector: 'app-crypto-token-form',
  templateUrl: './crypto-token-form.component.html',
  styleUrls: ['./crypto-token-form.component.scss']
})
export class CryptoTokenFormComponent implements OnInit {
  @Output() saved = new EventEmitter<IToken>();

  form!: FormGroup;
  blockchains = BLOCKCHAIN;

  get companyNameControl(): AbstractControl {
    return this.form.controls['companyName'];
  }

  get tokenNameControl(): AbstractControl {
    return this.form.controls['tokenName'];
  }

  get symbolControl(): AbstractControl {
    return this.form.controls['symbol'];
  }

  get maxTokenSupplyControl(): AbstractControl {
    return this.form.controls['maxTokenSupply'];
  }

  get formIsValid(): boolean {
    return this.form.valid;
  }

  constructor() { }

  ngOnInit(): void {
    this.form = new FormGroup({
      companyName: new FormControl('', [Validators.required]),
      tokenName: new FormControl('', [Validators.required]),
      symbol: new FormControl('', [Validators.required, Validators.pattern(/^[A-Z]{4}$/)]),
      maxTokenSupply: new FormControl('', [Validators.required, Validators.max(100000000)]),
      blockchain: new FormControl(this.blockchains.ENTHEREUM),
      isSupply: new FormControl(false, [Validators.requiredTrue]),
    })
  }

  submit() {
    console.log(this.form.value);
    this.saved.emit(this.form.value);
  }

}
