import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of, tap} from "rxjs";

export enum BLOCKCHAIN {
  ENTHEREUM = 'Enthereum',
  BINANCE = 'Binance Smart Chain'
}

export type BlockChain = BLOCKCHAIN;

export interface IToken {
  companyName: string;
  tokenName: string;
  symbol: string;
  maxTokenSupply: number;
  blockchain: BlockChain;
  isSupply: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CryptoTokenService {
  tokens$: BehaviorSubject<IToken[]> = new BehaviorSubject<IToken[]>([]);

  get observe(): Observable<any> {
    return this.tokens$.asObservable();
  }

  constructor() { }

  createToken(token: IToken): Observable<any> {
    return of([]).pipe(
      tap((tokens) => this.tokens$.next([...tokens, token]))
    );
  }
}
